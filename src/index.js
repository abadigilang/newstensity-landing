import React from 'react';
import ReactDOM from 'react-dom';
import Landing from './page/landing.jsx';

ReactDOM.render(<Landing />, document.getElementById('app'));
