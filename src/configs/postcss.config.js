module.exports = {
    plugins: {
      'postcss-import': {},
      'postcss-preset-env': {
        features: {
          rem: { rootValue: 10 }
        }
      }
    }
  };
  