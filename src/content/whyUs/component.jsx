import React from 'react';
import styles from './styles.scss';

class WhyUs {
  static components(props) {
    return new WhyUs().root() ;
  }

  root() {
    return (
      <div className={styles.why_us}>
        <div className={styles.why_us_wrap}>
          <h1>Why Us?</h1>
          {this.content()}
        </div>
      </div>
    );
  }

  content() {
    return (
      <div className={styles.why_us_content}>
        <section>
          <h2>Pioneer</h2>
          1st platform in integrating data from Mainstream & Social Media
        </section>
        <section>
          <h2>Unlimited</h2>
          <ul>
            <li>Unlimited data stream: Grabs more than 20k data within a day</li>
            <li>Unlimited Media: Crawl over 500 credible printed and online media</li>
            <li>Gather data from Print, Online, Broadcast & Social Media</li>
          </ul>
        </section>
        <section>
          <h2>Realtime</h2>
          <ul>
            <li>Pull data within 10 minutes</li>
            <li>Prevent a crisis with cut down on response time</li>
          </ul>
        </section>
        <section>
          <h2>Accurate</h2>
          <ul>
            <li>Our developed AI accurately assign sentiment on each data</li>
          </ul>
        </section>
        <section>
          <h2>Mapping</h2>
          <ul>
            <li>Better mapping campaigns to specific business objectives or organization</li>
            <li>Our ontology helps you find connected entity, most influence person and location related to you</li>
            <li>Gain market intelligence by tracking your competitors and key topics within your industry</li>
          </ul>
        </section>
      </div>
    );
  }
}

export default WhyUs.components;
