import React from 'react';
import styles from './styles.scss';

class OurBoard {
  static components(props) {
    return new OurBoard().root() ;
  }

  root() {
    return (
      <div className={styles.our_board}>
        <h1>Our Board</h1>
        <div className={styles.our_board_wrap}>
          <div className={styles.our_board_content}>
            <h2>Sapto Anggoro - CEO</h2>
            <p>Starting his career as a young journalist in Surabaya Post, until he decided to move to Jakarta as a journalist in Republika. He then countinued his carrier to Detik.com. He became a key journalist and hold deputy of chief journalist soon after. However, SAP as he often called not just known as a journalist. He sharpened his business skill as GM Sales and finally held position as Director of Operations (2011). Not Suprisingly, with an extensive network and familiar in business and oranizational activities he became Secretary General of APJII from 2012 to 2015. On 2016, he was became co-founder of Tirto.ID</p>
            <h2>Teguh Budi Santoso - COO</h2>
            <p>Started career as journalist since 1999, TBS - his famously known nickname, is full with editorial experience in almost all areas of reportage. Started from Entertainment news, Political events, Sports, Economy and Information Technology. At the end of his career at Detik.com, he was trusted to handle their content development business core. Then, he started a new challenge to initiate Binokular until now On 2016, he was became co-founder of Tirto.ID</p>
            <h2>Nur Samsi - CTO</h2>
            <p>He graduated from Computer Polytechnic IPB. He started his carrier in Detik.com. In a short period, he had become a leading programmer. To pursue and gain more experience, he then worked in various multinational companies in software development and deployment. He spent most of his time working on foreign projects, until he returned to raise Binokular.</p>
          </div>
        </div>
      </div>
    );
  }
}

export default OurBoard.components;
