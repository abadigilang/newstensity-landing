import React from 'react';
import styles from './styles.scss';

class Wrapper {
  static components(props) {
    return (
      <div className={styles.wrapper}>
        {props.children}
      </div>
    );
  }  
}

export default Wrapper.components;
