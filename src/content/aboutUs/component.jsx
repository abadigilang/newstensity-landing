import React from 'react';
import styles from './styles.scss';

class AboutUs {
  static components(props) {
    return new AboutUs().root() ;
  }

  root() {
    return (
      <div className={styles.about_us}>
        {this.aboutContent()}
        {this.trialCards()}
      </div>
    );
  }

  aboutContent() {
    return (
      <div className={styles.about_wrapper}>
        <h1 className={styles.jumbo_title}>Brings Data and Insight
        {"\n"}
        Directly to Your Sight</h1>
        <p className={styles.about_content}>
          <b>Newstensity</b> is a BIG DATA MONITORING tools, part of <b>Binokular Group</b>. Dashboard equipped with analysis tools, measurement, with user friendly visualization. The leveraging of machine learning and traditional algorithms to analyze the Big data for any organization can solve problems in multiple verticals. <b>Newstensity</b> use DEEP LEARNING methods based on Learning Data Representations (Data Sampling)
        </p>
      </div>
    );
  }

  trialCards() {
    return (
      <div className={styles.trial_cards}>
        {this.trialButton()}
        {this.trialContent()}
        {this.bgPlan()}
      </div>
    );
  }

  trialButton() {
    return (
      <div className={styles.ask_action_wrap}>
        <button>Ask for Trial</button>
        <div className={styles.ask_line} />
      </div>
    );
  }

  trialContent() {
    return (
      <div className={styles.trial_content_wrap}>
        <div className={styles.trial_content}>
          <p>
            "Things get done only if the data we
            {"\n"}
            gather can inform and inspire those in
            {"\n"}a position to make a difference."
          </p>
          <div>- Mike schmoker</div>
        </div>
      </div>
    );
  }

  bgPlan() {
    return <div className={styles.bg_plan}/>
  }
}

export default AboutUs.components;
